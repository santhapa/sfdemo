<?php 
namespace Demo\Bundle\PostBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PostFormType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder->add('title', 'text', array(
				'label' => 'Post Title',
				'required'=> false,
				'attr' => array('placeholder' => "Title of the post")
			))
			->add('content', 'textarea', array(
				'label' => 'Post Content',
				'required' => false,
				'attr' => array('placeholder' => 'Write some original content')
			))
			->add('author', 'text', array(
				'label'=> 'Author Name',
				'required' => false,
				'attr' => array('placeholder'=>'Author of the Post')
			))
			->add('add', 'submit')
			;
	}
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'Demo\Bundle\PostBundle\Entity\Post',
			'csrf_protection' => false,
			));
	}
	public function getName()
	{
		return 'demo_post';
	}
}
?>