<?php
// basic crud approach with route and template from annotation

namespace Demo\Bundle\PostBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration as Symfony;

use Demo\Bundle\PostBundle\Entity\Post;

class TemplateController extends Controller
{   
    /**
    * @Symfony\Route("index", name="demo_template_index")
    * @Symfony\Template()
    */
    public function indexAction()
    {
    	$em = $this->getDoctrine()->getManager();
    	$posts = $em->getRepository('DemoPostBundle:Post')->findAll();

        return array('posts' => $posts);
    }

    /**
    * @Symfony\Route("new", name="demo_template_new")
    * @Symfony\Template()
    */
    public function newAction(Request $request)
    {
    	$post = new Post;

    	if($request->getMethod() == 'POST')
    	{
    		$title = $request->request->get('title');
    		$content = $request->request->get('content');
    		$author = $request->request->get('author');
    		$createdAt = new \DateTime();

    		$post->setTitle($title);
    		$post->setContent($content);
    		$post->setAuthor($author);
    		$post->setCreatedAt($createdAt);

    		$em = $this->getDoctrine()->getManager();
    		$em->persist($post);
    		$em->flush();

    		return $this->redirectToRoute('demo_template_index');
    	}

    	return array('post'=>$post);

    }

    /**
    * @Symfony\Route("edit/{id}", name="demo_template_edit")
    * @Symfony\Template()
    */
    public function editAction(Request $request, $id)
    {
    	$em = $this->getDoctrine()->getManager();

    	$post = $em->getRepository('DemoPostBundle:Post')->find($id);

    	if(!$post) throw $this->createNotFoundException('Post not found.');    	

    	if($request->getMethod() == "POST")
    	{
    		$title = $request->request->get('title');
    		$content = $request->request->get('content');
    		$author = $request->request->get('author');
    		$createdAt = new \DateTime();

    		$post->setTitle($title);
    		$post->setContent($content);
    		$post->setAuthor($author);
    		$post->setCreatedAt($createdAt);

    		$em = $this->getDoctrine()->getManager();
    		$em->flush();

    		return $this->redirectToRoute('demo_template_index');
    	}

    	return array('post'=>$post);
    }

    /**
    * @Symfony\Route("delete/{id}", name="demo_template_delete")
    * @Symfony\Template()
    */
    public function deleteAction($id)
    {
    	$em = $this->getDoctrine()->getManager();

    	$post = $em->getRepository('DemoPostBundle:Post')->find($id);

    	if(!$post) throw $this->createNotFoundException('Post not found.');    

    	$em->remove($post);
    	$em->flush();

    	return $this->redirectToRoute('demo_template_index');

    }
}
