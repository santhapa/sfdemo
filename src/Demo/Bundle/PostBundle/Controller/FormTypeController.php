<?php
// basic crud approach with form type and form as service

namespace Demo\Bundle\PostBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use Demo\Bundle\PostBundle\Entity\Post;
use Demo\Bundle\PostBundle\Form\Type\PostFormType;

class FormTypeController extends Controller
{
    public function indexAction()
    {
    	$em = $this->getDoctrine()->getManager();
    	$posts = $em->getRepository('DemoPostBundle:Post')->findAll();

        return $this->render('DemoPostBundle:FormType:index.html.twig', array('posts' => $posts));
    }

    public function newAction(Request $request)
    {
    	$post = new Post;

        $form = $this->createForm(new PostFormType(), $post);

        $form->handleRequest($request);

    	if($form->isValid())
    	{
    		$createdAt = new \DateTime();
    		$post->setCreatedAt($createdAt);

        	$em = $this->getDoctrine()->getManager();
    		$em->persist($post);
    		$em->flush();

    		return $this->redirectToRoute('demo_form_type_index');
    	}

    	return $this->render('DemoPostBundle:FormType:new.html.twig', array(
            'post'=>$post,
            'form' => $form->createView()
        ));

    }

    public function editAction(Request $request, $id)
    {
    	$em = $this->getDoctrine()->getManager();

    	$post = $em->getRepository('DemoPostBundle:Post')->find($id);

    	if(!$post) throw $this->createNotFoundException('Post not found.');    	

    	$form = $this->createForm('demo_post',$post);

        $form->handleRequest($request);

        if($form->isValid())
        {
    		$em->flush();

            return $this->redirectToRoute('demo_form_type_index');
        }

    	return $this->render('DemoPostBundle:FormType:edit.html.twig', array(
            'post'=>$post,
            'form' => $form->createView()
        ));
    }

    public function deleteAction($id)
    {
    	$em = $this->getDoctrine()->getManager();

    	$post = $em->getRepository('DemoPostBundle:Post')->find($id);

    	if(!$post) throw $this->createNotFoundException('Post not found.');    

    	$em->remove($post);
    	$em->flush();

    	return $this->redirectToRoute('demo_form_type_index');

    }
}
