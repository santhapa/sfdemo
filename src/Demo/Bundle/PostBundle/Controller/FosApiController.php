<?php
namespace Demo\Bundle\PostBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations as Rest;

use Symfony\Component\HttpFoundation\Request;

use Demo\Bundle\PostBundle\Entity\Post;

class FosApiController extends FOSRestController
{
    public function getPostsAction()
    {
        $posts = $this->getDoctrine()->getManager()->getRepository('DemoPostBundle:Post')->findAll();

        $view = $this->view($posts, 200)
            ->setTemplate("DemoPostBundle:Api:index.html.twig")
            ->setTemplateVar('posts')
        ;

        return $this->handleView($view);
    }

    public function newPostAction(Request $request)
    {
        $post = new Post();

        // $form = $this->createForm('demo_post', $post);
        $form = $this->get('form.factory')->createNamed('', "demo_post", $post);

        $form->bind($request);

        if($form->isValid())
        {
            $post->setCreatedAt(new \DateTime);
            $em = $this->getDoctrine()->getManager();
            $em->persist($post);
            $em->flush();

            // return $this->view($post, Codes::HTTP_CREATED);
            return $this->routeRedirectView('demo_api_all', array(), Codes::HTTP_CREATED);
        }else{
            $view = $this->view($form, Codes::HTTP_BAD_REQUEST )
            ->setTemplate("DemoPostBundle:Api:new.html.twig")
            ->setTemplateVar('form');
        }

        return $this->handleView($view);
    }

    /**
    * @Rest\RequestParam(name="title", requirements="[a-zA-Z0-9]+", description="Title of the Post")
    * @Rest\RequestParam(name="content", requirements=".+", description="Content of the Post")
    * @Rest\RequestParam(name="author", requirements="[a-zA-Z]+", description="Content of the Post")
    */
    public function __newPostAction(ParamFetcher $pf)
    {
        $title = $pf->get('title');
        $content = $pf->get('content');
        $author = $pf->get('author');

        $post = new Post();
        $post->setTitle($title);
        $post->setContent($content);
        $post->setAuthor($author);
        $post->setCreatedAt(new \DateTime());

        $em = $this->getDoctrine()->getManager();
        $em->persist($post);
        $em->flush();

         $view = $this->view($post, Codes::HTTP_CREATED);
        // $view = $this->routeRedirectView('demo_api_all', array(), Codes::HTTP_CREATED);

        return $this->handleView($view);
    }
}
