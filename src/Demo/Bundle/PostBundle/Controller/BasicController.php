<?php
// basic crud approach with rendering view twig

namespace Demo\Bundle\PostBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use Demo\Bundle\PostBundle\Entity\Post;

class BasicController extends Controller
{
    public function indexAction()
    {
    	$em = $this->getDoctrine()->getManager();
    	$posts = $em->getRepository('DemoPostBundle:Post')->findAll();

        return $this->render('DemoPostBundle:Basic:index.html.twig', array('posts' => $posts));
    }

    public function newAction(Request $request)
    {
    	$post = new Post;

    	if($request->getMethod() == 'POST')
    	{
    		$title = $request->request->get('title');
    		$content = $request->request->get('content');
    		$author = $request->request->get('author');
    		$createdAt = new \DateTime();

    		$post->setTitle($title);
    		$post->setContent($content);
    		$post->setAuthor($author);
    		$post->setCreatedAt($createdAt);

    		$em = $this->getDoctrine()->getManager();
    		$em->persist($post);
    		$em->flush();

    		return $this->redirectToRoute('demo_basic_index');
    	}

    	return $this->render('DemoPostBundle:Basic:new.html.twig', array('post'=>$post));

    }

    public function editAction(Request $request, $id)
    {
    	$em = $this->getDoctrine()->getManager();

    	$post = $em->getRepository('DemoPostBundle:Post')->find($id);

    	if(!$post) throw $this->createNotFoundException('Post not found.');    	

    	if($request->getMethod() == "POST")
    	{
    		$title = $request->request->get('title');
    		$content = $request->request->get('content');
    		$author = $request->request->get('author');
    		$createdAt = new \DateTime();

    		$post->setTitle($title);
    		$post->setContent($content);
    		$post->setAuthor($author);
    		$post->setCreatedAt($createdAt);

    		$em = $this->getDoctrine()->getManager();
    		$em->flush();

    		return $this->redirectToRoute('demo_basic_index');
    	}

    	return $this->render('DemoPostBundle:Basic:edit.html.twig', array('post'=>$post));
    }

    public function deleteAction($id)
    {
    	$em = $this->getDoctrine()->getManager();

    	$post = $em->getRepository('DemoPostBundle:Post')->find($id);

    	if(!$post) throw $this->createNotFoundException('Post not found.');    

    	$em->remove($post);
    	$em->flush();

    	return $this->redirectToRoute('demo_basic_index');

    }
}
