<?php
// basic crud approach with validation normal approach

namespace Demo\Bundle\PostBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use Demo\Bundle\PostBundle\Entity\Post;

class ValidationController extends Controller
{
    public function indexAction()
    {
    	$em = $this->getDoctrine()->getManager();
    	$posts = $em->getRepository('DemoPostBundle:Post')->findAll();

        return $this->render('DemoPostBundle:Validation:index.html.twig', array('posts' => $posts));
    }

    public function newAction(Request $request)
    {
    	$post = new Post;

        $errors = null;
        $rawErrors = null;

    	if($request->getMethod() == 'POST')
    	{
    		$title = $request->request->get('title');
    		$content = $request->request->get('content');
    		$author = $request->request->get('author');
    		$createdAt = new \DateTime();

    		$post->setTitle($title);
    		$post->setContent($content);
    		$post->setAuthor($author);
    		$post->setCreatedAt($createdAt);

            $validator = $this->get('validator');
            $errorsList = $validator->validate($post);

            if(count($errorsList) == 0)
            {
        		$em = $this->getDoctrine()->getManager();
        		$em->persist($post);
        		$em->flush();

        		return $this->redirectToRoute('demo_validation_index');
            }else{
                $rawErrors = (string) $errorsList;
                $errors = $errorsList;
            }
    	}

    	return $this->render('DemoPostBundle:Validation:new.html.twig', array(
            'post'=>$post, 
            'errors'=>$errors,
            'rawErrors'=>$rawErrors,
        ));

    }

    public function editAction(Request $request, $id)
    {
    	$em = $this->getDoctrine()->getManager();

    	$post = $em->getRepository('DemoPostBundle:Post')->find($id);
        $rawErrors = null;
        $errors = null;

    	if(!$post) throw $this->createNotFoundException('Post not found.');    	

    	if($request->getMethod() == "POST")
    	{
    		$title = $request->request->get('title');
    		$content = $request->request->get('content');
    		$author = $request->request->get('author');
    		$createdAt = new \DateTime();

    		$post->setTitle($title);
    		$post->setContent($content);
    		$post->setAuthor($author);
    		$post->setCreatedAt($createdAt);

            $validator = $this->get('validator');
            $errorsList = $validator->validate($post);

            if(count($errorsList) == 0)
            {
                $em = $this->getDoctrine()->getManager();
                $em->flush();

                return $this->redirectToRoute('demo_validation_index');
            }else{
                $rawErrors = (string) $errorsList;
                $errors = $errorsList;
            }
    	}

    	return $this->render('DemoPostBundle:Validation:edit.html.twig', array(
            'post'=>$post, 
            'errors'=>$errors,
            'rawErrors'=>$rawErrors,
        ));
    }

    public function deleteAction($id)
    {
    	$em = $this->getDoctrine()->getManager();

    	$post = $em->getRepository('DemoPostBundle:Post')->find($id);

    	if(!$post) throw $this->createNotFoundException('Post not found.');    

    	$em->remove($post);
    	$em->flush();

    	return $this->redirectToRoute('demo_validation_index');

    }
}
