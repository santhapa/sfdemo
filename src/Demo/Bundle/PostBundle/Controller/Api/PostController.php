<?php
namespace Demo\Bundle\PostBundle\Controller\Api;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations as Rest;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Demo\Bundle\PostBundle\Entity\Post;

class PostController extends FOSRestController
{
    // [GET] /posts
    // get_posts
    public function getPostsAction()
    {
        $posts = $this->getDoctrine()->getManager()->getRepository('DemoPostBundle:Post')->findAll();

        $view = $this->view($posts, Codes::HTTP_OK)
            ->setTemplate("DemoPostBundle:Api:index.html.twig")
            ->setTemplateVar('posts')
        ;

        return $this->handleView($view);
    }

    // [GET] /posts/new
    // new_posts
    public function newPostsAction(Request $request)
    {
        $post = new Post();
        // $form = $this->createForm('demo_post', $post);
        $form = $this->get('form.factory')->createNamed('', "demo_post", $post);

        $view = $this->view($form, Codes::HTTP_OK )
            ->setTemplate("DemoPostBundle:Api:new.html.twig")
            ->setTemplateVar('form');

        return $this->handleView($view);
    }

    // [POST] /posts
    // post_posts
    public function postPostsAction(Request $request)
    {
        $post = new Post();
        // $form = $this->createForm('demo_post', $post);
        $form = $this->get('form.factory')->createNamed('', "demo_post", $post);

        $form->bind($request);

        if($form->isValid())
        {
            $post->setCreatedAt(new \DateTime);
            $em = $this->getDoctrine()->getManager();
            $em->persist($post);
            $em->flush();

            // return $this->view($post, Codes::HTTP_CREATED);
            return $this->routeRedirectView('demo_api_get_posts', array(), Codes::HTTP_CREATED);
        }else{
            $view = $this->view($form, Codes::HTTP_BAD_REQUEST )
            ->setTemplate("DemoPostBundle:Api:new.html.twig")
            ->setTemplateVar('form');
        }

        return $this->handleView($view);
    }

    // [GET] /posts/{id}
    // get_post
    public function getPostAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $post = $em->getRepository('DemoPostBundle:Post')->find($id);

        if(!$post)
            throw new HttpException(Codes::HTTP_BAD_REQUEST, "Post not found.");

        $view = $this->view($post, Codes::HTTP_OK)
            ->setTemplate("DemoPostBundle:Api:single.html.twig")
            ->setTemplateVar('post');

        return $this->handleView($view);
    }

    // [GET] /posts/{id}/edit
    // edit_post 
    public function editPostAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $post = $em->getRepository('DemoPostBundle:Post')->find($id);
        
        if(!$post)
            throw new HttpException(Codes::HTTP_BAD_REQUEST, "Post not found.");

        $form = $this->get('form.factory')->createNamed('', "demo_post", $post);

        $view = $this->view($form, Codes::HTTP_OK)
            ->setTemplate("DemoPostBundle:Api:edit.html.twig")
            ->setTemplateVar('form')
            ->setTemplateData(array('post'=>$post));

        return $this->handleView($view);
    }

    // [POST] /posts/{id}
    // post_post
    public function postPostAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $post = $em->getRepository('DemoPostBundle:Post')->find($id);

        if(!$post)
            throw new HttpException(Codes::HTTP_BAD_REQUEST, "Post not found.");

        $form = $this->get('form.factory')->createNamed('', "demo_post", $post);
        $form->bind($request);

        if($form->isValid())
        {
            $em->flush();
            // return $this->view($post, Codes::HTTP_CREATED);
            return $this->routeRedirectView('demo_api_get_posts', array(), Codes::HTTP_NO_CONTENT);
        }else{
            $view = $this->view($form, Codes::HTTP_BAD_REQUEST )
            ->setTemplate("DemoPostBundle:Api:edit.html.twig")
            ->setTemplateVar('form')
            ->setTemplateData(array('post'=>$post));
        }

        return $this->handleView($view);
    }

    // [PUT] /posts/{id}
    // put_post 
    public function putPostAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $post = $em->getRepository('DemoPostBundle:Post')->find($id);

        if(!$post)
            throw new HttpException(Codes::HTTP_BAD_REQUEST, "Post not found.");

        $form = $this->get('form.factory')->createNamed('', "demo_post", $post);
        $form->bind($request);

        if($form->isValid())
        {
            $em->flush();
            // return $this->view($post, Codes::HTTP_CREATED);
            return $this->routeRedirectView('demo_api_get_posts', array(), Codes::HTTP_NO_CONTENT);
        }else{
            $view = $this->view($form, Codes::HTTP_BAD_REQUEST )
            ->setTemplate("DemoPostBundle:Api:edit.html.twig")
            ->setTemplateVar('form')
            ->setTemplateData(array('post'=>$post));
        }

        return $this->handleView($view);
    }

    // [DELETE] /posts/{id}
    // delete_post
    public function deletePostAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $post = $em->getRepository('DemoPostBundle:Post')->find($id);

        if(!$post)
            throw new HttpException(Codes::HTTP_BAD_REQUEST, "Post not found.");

        $em->remove($post);
        $em->flush();

        return $this->routeRedirectView('demo_api_get_posts', array(), Codes::HTTP_NO_CONTENT);
    }

    // [GET] /posts/{id}/remove
    // remove_post
    public function removePostAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $post = $em->getRepository('DemoPostBundle:Post')->find($id);

        if(!$post)
            throw new HttpException(Codes::HTTP_BAD_REQUEST, "Post not found.");

        $em->remove($post);
        $em->flush();

        return $this->routeRedirectView('demo_api_get_posts', array(), Codes::HTTP_NO_CONTENT);
    }
}
